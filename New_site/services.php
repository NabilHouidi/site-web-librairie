<?php 
    $currentPage = 'services';
    include "./templates/header.php";
    ?>



    <!-- Services item -->
    <section class="flat-row section-iconbox padding2 bg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="title-section style3 left">
                        <h1 class="title">Our Industry</h1>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="title-section padding-left50">
                        <div class="sub-title style3">
                            We help organisations work smarter and grow faster. Reach out to us to build effective organisations, reduce costs, manage risk and regulation and leverage talent.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-pie-chart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Corporate Finance</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div>
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-bar-chart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Economic Consulting</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div> 
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-bell"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Forensic & Litigation</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div> 
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-microphone"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Strategic Communications</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div> 
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-desktop"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Technology Consulting</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div> 
                <div class="col-lg-4">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-support"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Healthcare Consulting</h5>  
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p> 
                        </div>
                    </div>     
                </div>       
            </div>  
        </div>
    </section> 

    <section class="flat-row section-testimonials2 padding2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">  
                    <div class="title-section text-center">
                        <div class="symbol">
                           ​‌“ 
                        </div>
                        <h1 class="cd-headline clip is-full-width">
                            <span>2000+</span>
                            <span class="cd-words-wrapper">
                                <b class="is-visible"> Happy Client</b>
                                <b>Believe Client</b>
                                <b>Choice Client</b>
                            </span>
                        </h1>
                    </div>      
                </div>
                <div class="col-md-12">
                    <div class="wrap-testimonial padding-lr79">
                        <div id="testimonial-slider">
                            <ul class="slides">
                                <li>
                                    <div class="testimonials style3 text-center"> 
                                        <div class="message">                                
                                            <blockquote class="whisper">
                                               "We love the approachable format, and the fact that they chose to feature customers that users can really relate to.Each client story module links to the client's website, Facebook page, and app in both the Android and Apple app stores and sets people up for success."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Shaya Hill</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span class="position">Tax Advice</span>
                                        </div>                      
                                    </div>
                                </li>
                               <li>
                                    <div class="testimonials style3 text-center"> 
                                        <div class="message">                                
                                            <blockquote class="whisper">
                                               " We worked with Consuloan. Our representative was  very knowledgeable and helpful. Consuloan made a number of suggestions to help improve our systems. Consuloan explained how things work and why it would help. We are pleased with the result and we definitely highly recommend Consuloan."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Alex Poole</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span class="position">CEO DeerCreative</span>
                                        </div>                      
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonials style3 text-center"> 
                                        <div class="message">                                
                                            <blockquote class="whisper">
                                               " Even though I am a seasoned business owner myself, I am sure that there’s always room for growth, inspiration, and new ideas.It's has provided a common language that is gaining popularity in the workplace as it creates new learning and sets people up for success."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Anthony Jones</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span class="position">Business Planner</span>
                                        </div>                      
                                    </div>
                                </li>
                            </ul>
                        </div>
                            
                        <div id="testimonial-carousel">
                            <ul class="slides clearfix">
                                <li>
                                    <img alt="image" src="images/testimonial/1.jpg">
                                </li>  
                                <li>
                                    <img alt="image" src="images/testimonial/2.jpg">
                                </li> 
                                <li>
                                    <img alt="image" src="images/testimonial/3.jpg">
                                </li>    
                            </ul>
                        </div>
                    </div><!-- /.wrap-testimonial -->
                </div>
            </div>
        </div>
    </section>

    <section class="flat-row background-nopara background-image1 section-counter">
        <div class="section-overlay style2"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">  
                    <div class="title-section style2 color-white text-center">
                        <h1 class="title">We advise you, you call <span>the right</span> decision!</h1>
                        <div class="sub-title">
                            We help entrepreneurs get their act together before they talk to investors.
                        </div>
                    </div>          
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row --> 
            <div class="row">
              <div class="col-lg-3 col-sm-6">
                  <div class="flat-counter text-center">                            
                    <div class="content-counter">
                        <div class="icon-count">
                            <i class="ti-pie-chart"></i>
                        </div>
                        <div class="numb-count" data-to="90" data-speed="2000" data-waypoint-active="yes">90</div>
                        <div class="name-count">Companies consulted</div>
                    </div>
                </div><!-- /.flat-counter -->
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="flat-counter text-center">                            
                    <div class="content-counter">
                        <div class="icon-count">
                            <i class="ti-headphone-alt"></i>
                        </div>
                        <div class="numb-count" data-to="120" data-speed="2000" data-waypoint-active="yes">120</div>
                        <div class="name-count">Consultants</div>
                    </div>
                </div><!-- /.flat-counter -->
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="flat-counter text-center">                            
                    <div class="content-counter">
                        <div class="icon-count">
                            <i class="ti-bookmark-alt"></i>
                        </div>
                        <div class="numb-count" data-to="50" data-speed="2000" data-waypoint-active="yes">50</div>
                        <div class="name-count">Awards Winning</div>
                    </div>
                </div><!-- /.flat-counter -->
              </div>
              <div class="col-lg-3 col-sm-6">
                  <div class="flat-counter text-center">                            
                    <div class="content-counter">
                        <div class="icon-count">
                            <i class="ti-heart-broken"></i>
                        </div>
                        <div class="numb-count" data-to="240" data-speed="2000" data-waypoint-active="yes">240</div>
                        <div class="name-count">Satisfied Customers</div>
                    </div>
                </div><!-- /.flat-counter -->
              </div>
          </div>          
        </div><!-- /.container --> 
    </section> 


<?php 
    include "./templates/footer.php";
    ?>