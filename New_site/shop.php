<?php 
    $currentPage = 'shop';
    include "./templates/header.php";
    include "./php/category-class.php";
    include "./php/product-class.php";
    include "./php/db.php";

    if (isset($_GET['category']))
    {
        $productsArray = Product::fetchByCategoryID($_GET['category']);
    }
    else
    {
        $productsArray = Product::fetchAll();
    }

    $categoriesArray = Category::fetchAll();
    
    
    ?>

<section class="flat-row section-product2">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="sidebar shop">
                    <div class="widget widget-nav-menu">
                        <ul class="widget-menu">
                            <li class="active">
                                <a href="#">Categories</a>
                            </li>
                            <?php $i = sizeof($categoriesArray); while($i--): ?>
                                <li>                               
                                    <a href=<?= "shop.php?category=" . $categoriesArray[$i]['CategoryId'];?>> <?=$categoriesArray[$i]['CategoryName'];?> </a>
                                </li>
                            <?php endwhile ;?>

                        </ul>
                    </div>
                    <div class="widget widget-shopby">
                        <h5 class="widget-title">Filtre par prix</h5>
                        <form>
                            <div class="price_slider_wrapper">
                                <div class="price_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header"></div>
                                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#">
                                        <span class="shadow dow"></span>
                                    </a>
                                    <a class="ui-slider-handle ui-state-default ui-corner-all" href="#">
                                        <span class="shadow dow active"></span>
                                    </a>
                                </div>
                            </div>
                        </form>
                        <div class="price_slider_amount">
                            <div class="price_label">
                                <label>Prix:</label>
                                Put dynamic slider here
                                <input type="text">
                            </div>
                        </div>
                        <a href="#" class="flat-button style2">Filtrer</a>
                    </div>
                    <div class="widget widget-product">
                        <h5 class="widget-title">Latest Products</h5>
                        <ul class="latest">
                            <li class="clearfix">
                                <div class="product-image float-left">
                                    <a href=<?= "shop-single.php?pid=" //. $product->getProductId(); ?> >
                                        <img src="images/product/c1.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-shop">
                                    <h6 class="product-name">
                                        <a href="#">Produit </a>
                                    </h6>
                                    <div class="price-box">
                                        <ins>
                                            <span class="amount">200 Dt</span>
                                        </ins>
                                    </div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="product-image float-left">
                                    <a href="shop-single.html">
                                        <img src="images/product/c2.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-shop">
                                    <h6 class="product-name">
                                        <a href="#">Produit </a>
                                    </h6>
                                    <div class="price-box">
                                        <ins>
                                            <span class="amount">200 Dt</span>
                                        </ins>
                                    </div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="product-image float-left">
                                    <a href="shop-single.html">
                                        <img src="images/product/c3.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="product-shop">
                                    <h6 class="product-name">
                                        <a href="#">Produit </a>
                                    </h6>
                                    <div class="price-box">
                                        <ins>
                                            <span class="amount">200 Dt</span>
                                        </ins>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                <div class="col-lg-9">
                    
                    <div class="wrap-product clearfix">
                        <!-- Product card-->
                        <?php foreach($productsArray as $product):?>
                        <div class="product style2">
                            <div class="box-product">
                                <div class="featured-product">
                                    <a href=<?= "shop-single.php?pid=" . $product->getProductId(); ?>>
                                        <img src=<?= $product->getProductMainImage()   ;?> alt="image">
                                    </a>
                                </div>
                                <div class="content-product text-center">
                                    <div class="name">
                                        <span><?= $product->getProductName()  ;?> </span>
                                    </div>
                                    
                                    <? $product->DisplayProductPrice() ; ?>
                                    
                                    <div class="btn-card">
                                        <a href=<?= "shop-single.php?pid=" . $product->getProductId() ;?> class="flat-button style2">Plus de détails</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ;?>
                        <!-- /Product card-->
                    </div>
                </div>
            </div>
        </div>
</section>

<?php 
    include "./templates/footer.php";
?>