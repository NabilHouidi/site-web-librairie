 
<footer class="footer widget-footer">
      <div class="container">
           <div class="row">
               <div class="col-lg-3 col-sm-6 reponsive-mb30">
                   <div class="widget-logo">
                       <div id="logo-footer" class="logo">
                           <a href="index.html" rel="home">
                              <img src="images/logo1.png" alt="image">
                           </a>
                       </div>
                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
                       <ul class="flat-information">
                           <li><i class="fa fa-map-marker"></i><a href="#">address, Tunis, TN</a></li>
                           <li><i class="fa fa-phone"></i><a href="#">(+216-77-500-100)</a></li>
                           <li><i class="fa fa-envelope"></i><a href="#">contact.librairie@gmail.com</a></li>
                       </ul>
                   </div>
               </div><!-- /.col-md-3 -->

               <div class="col-lg-3 col-sm-6 reponsive-mb30">
                   <div class="widget widget-out-link clearfix">
                       <h5 class="widget-title">Our Links</h5>
                       <ul class="one-half">
                           <li><a href="index.php">Accueil</a></li>
                           <li><a href="about-us.php">A Propos</a></li>
                           <li><a href="services.php">Nos Services</a></li>
                       </ul>
                       <ul class="one-half">
                           <li><a href="shop.php">Nos Articles</a></li>
                           <li><a href="contact.php">Contact</a></li>
                       </ul>
                   </div>
               </div><!-- /.col-md-3 -->

               <div class="col-lg-3 col-sm-6 reponsive-mb30">
             <!--     <div class="widget widget-recent-new">
                       <h5 class="widget-title">Recent News</h5>
                       <ul class="popular-new">
                           <li>
                              <div class="text">
                                   <h6><a href="#">Colombia Gets a Business Makeover</a></h6>
                                   <span>20 Aug 2017</span>
                              </div>
                           </li>
                           <li>
                              <div class="text">
                                   <h6><a href="#">Counting the Cost of Delay & Disruption</a></h6>
                                   <span>20 Aug 2017</span>
                              </div>
                           </li>
                       </ul>
                    </div>< /.col-md-3 -->
                   </div>

               <div class="col-lg-3 col-sm-6 reponsive-mb30">
                   <div class="widget widget-letter">
                       <h5 class="widget-title">Newsletter</h5>
                       <p class="info-text">Inscription à notre newsletter pour savoir les dernières offres.</p>
                       <form id="subscribe-form" class="flat-mailchimp" method="post" action="#" data-mailchimp="true">
                           <div class="field clearfix" id="subscribe-content">
                              <p class="wrap-input-email">
                                   <input type="text" tabindex="2" id="subscribe-email" name="subscribe-email" placeholder="Votre Email">
                              </p>
                              <p class="wrap-btn">
                                   <button type="button" id="subscribe-button" class="flat-button subscribe-submit" title="Subscribe now">Inscription</button>
                              </p>
                           </div>
                           <div id="subscribe-msg"></div>
                       </form>
                   </div>
               </div><!-- /.col-md-3 -->
           </div><!-- /.row -->
      </div><!-- /.container -->
  </footer>
    <!-- Bottom -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copyright">
                        <p>2018 <a href="https://ensi-junior-entreprise.com">ENSI Junior Entreprise</a>. All rights reserved.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="social-links style2 text-right">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- bottom -->

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>
    </div>

    <!-- Javascript -->
    <script src="javascript/jquery.min.js"></script>
    <script src="javascript/tether.min.js"></script>
    <script src="javascript/bootstrap.min.js"></script>
    <script src="javascript/jquery.easing.js"></script>
    <script src="javascript/jquery-waypoints.js"></script>
    <script src="javascript/jquery-validate.js"></script>
    <script src="javascript/jquery.cookie.js"></script>
    <script src="javascript/owl.carousel.js"></script>
    <script src="javascript/jquery.flexslider-min.js"></script>
    <script src="javascript/modern.custom.js"></script>
    <script src="javascript/jquery.hoverdir.js"></script>
    <script src="javascript/gmap3.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtwK1Hd3iMGyF6ffJSRK7I_STNEXxPdcQ&region=GB"></script>
    <script src="javascript/parallax.js"></script>

    <script src="javascript/main.js"></script>

    <!-- Revolution Slider -->
    <script src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="revolution/js/slider5.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
</body>
</html>
