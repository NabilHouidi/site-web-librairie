<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Librairie MALAK</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color6.css" id="color6">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/animate.css">

    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">

    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="icon" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="icon">
    <link href="icon/favicon.ico" rel="icon">

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head>
<body class="header_sticky">
    <!-- Preloader -->
    <div id="loading-overlay">
        <div class="loader"></div>
    </div>

    <!-- Boxed -->
    <div class="boxed">
    <div class="flat-header-wrap">
        <!-- Header -->
        <header id="header" class="header header-style4 box-shadow1 header-classic bg-white clearfix">
            <div id="logo" class="logo float-left">
                <a href="index.php" rel="home">
                    <img src="images/logo11.jpg" alt="image">
                </a>
            </div><!-- /.logo -->
            <div class="flat-wrap-header float-right">
                <div class="nav-wrap clearfix">
                    <ul class="menu menu-extra style2 color-661 margin-left37 float-right">
                        <li id="s" class="show-search">
                            <a href="#search" class="flat-search"><i class="fa fa-search"></i></a>
                        </li>
                    </ul>
                    <aside class="widget widget-phone margin-left53 margin-top36 float-right">
                        <i class="fa fa-phone"></i><span>(+216)-77-500-100</span>
                    </aside>
                    <nav id="mainnav" class="mainnav style2 color-661 float-right">
                        <ul class="menu">
                            <li class="<?php if($currentPage == 'index' ){ echo 'active';}?>">
                                <a href="index.php">Accueil</a>
                            </li>
                            <li class="<?php if($currentPage == 'about' ){ echo 'active';}?>">
                                <a href="about-us.php">A Propos</a></li>
                            <li class="<?php if($currentPage == 'services' ){ echo 'active';}?>">
                                <a href="services.php">Nos Services</a></li>
                            <li class="<?php if($currentPage == 'shop' ){ echo 'active';}?>" >
                                <a href="shop.php">Nos Articles</a></li>
                            <li class="<?php if($currentPage == 'contact' ){ echo 'active';}?>" >
                                <a href="contact.php">Contact</a></li>
                        </ul><!-- /.menu -->
                    </nav><!-- /.mainnav -->
                    <div class="top-search">
                        <form action="#" id="searchform-all" method="get">
                            <div>
                                <input type="text" id="input-search" class="sss" placeholder="Search...">
                                <input type="submit" id="searchsubmit">
                            </div>
                        </form>
                    </div>
                    <div class="btn-menu">
                        <span></span>
                    </div><!-- //mobile menu button -->
                </div><!-- /.nav-wrap -->

            </div>
        </header>
        </div>
