 <?php 
    $currentPage = 'index';
    include "./templates/header.php";

    ?>

<div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container slide-overlay" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

<!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
<div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
    <div class="slotholder"></div>

        <ul><!-- SLIDE  -->

            <!-- SLIDE 1 -->
            <li data-index="rs-3049" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- <div class="overlay">
                </div> -->
                <!-- MAIN IMAGE -->
                <img src="images/slides/77.jpeg"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 12 -->
                <div class="tp-caption title-slide"
                    id="slide-3049-layer-1"
                    data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-102','-102']"
                    data-fontsize="['60','60','50','33']"
                    data-lineheight="['70','70','50','35']"
                    data-fontweight="['700','700','700','700']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                    data-textAlign="['left','left','left','left']"
                    data-paddingtop="[10,10,10,10]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 16; white-space: nowrap;">Librairie MALAK
                </div>

                 <!-- LAYER NR. 12 -->
                <div class="tp-caption sub-title color-661 position"
                    id="slide-3049-layer-3"
                    data-x="['left','left','left','left']" data-hoffset="['35','25','50','50']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-142','-142','-142','-142']"
                    data-fontsize="['16',16','16','14']"
                    data-fontweight="['600','600','600','600']"
                    data-width="['660','660','660','300']"
                    data-height="none"
                    data-whitespace="nowrap"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                    data-textAlign="['left','left','left','left']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 17; white-space: nowrap;text-transform:left;">Bienvenue a notre site web
                </div>

                <!-- LAYER NR. 13 -->
                <div class="tp-caption sub-title color-661"
                    id="slide-3049-layer-4"
                    data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-8','-8','-8,'-8']"
                    data-fontsize="['18',18','18','16']"
                    data-lineheight="['30','30','22','16']"
                    data-fontweight="['400','400','400','400']"
                    data-width="['800',800','800','450']"
                    data-height="none"
                    data-whitespace="['nowrap',normal','normal','normal']"

                    data-type="text"
                    data-responsive_offset="on"

                    data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                    data-textAlign="['left','left','left','left']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 17; white-space: normal;"> Toujours a votre service <br> Notre but est de satisfaire vos besoins

                </div>

                <a href="#" target="_self" class="tp-caption flat-button color-white text-center"

                data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                data-x="['center','center','center','center']" data-hoffset="['-502','-400','-240','-70']"
                data-y="['middle','middle','middle','middle']" data-voffset="['86','86','80','70']"
                data-fontweight="['700','700','700','700']"
                data-width="['auto']"
                data-height="['auto']"
                style="z-index: 3;">Nos Articles
                </a><!-- END LAYER LINK -->

                <a href="#" target="_self" class="tp-caption flat-button color-white text-center"

                data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                data-x="['center','center','center','center']" data-hoffset="['-318','-200','-30','-70']"
                data-y="['middle','middle','middle','middle']" data-voffset="['86','86','80','130']"
                data-fontweight="['700','700','700','700']"
                data-width="['auto']"
                data-height="['auto']"
                style="z-index: 3;">Nos Sevices
                </a><!-- END LAYER LINK -->
            </li>

        </ul>
</div>
</div><!-- END REVOLUTION SLIDER -->

<section class="flat-row section-icon">
        <div class="container">
            <ul id="data-effect" class="data-effect wrap-iconbox margin-top_121 clearfix">
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_globe"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Tirage</h5>
                            <p>Tirage tous formats de A4 à A0 </p>
                        </div>
                        <div class="effecthover">
                            <img src="images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_search-2"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Force 2</h5>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan doloremque laudantium</p>
                        </div>
                        <div class="effecthover">
                            <img src="images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_cogs"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Force 3</h5>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan doloremque laudantium</p>
                        </div>
                        <div class="effecthover">
                            <img src="images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
            </ul>
            
            </ul>
            <div class="divider sh72"></div>
            </div>
    </section>

    <section class="flat-row v12 parallax parallax5">
        <div class="section-overlay style2"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="infomation-text padding-lr140 text-center">Satisfaire nos clients est notre devoir <span>N°1</span>.</h1>
                </div>
            </div>
        </div>
    </section>

     <section class="section-maps-form wrap-blance blancejqurey2  parallax parallax4 clearfix">
        <div class="section-overlay style2"></div>
        <div id="blance-s1" class="one-half flat-maps-form1">
            <div class="flat-maps">
                <div id="maps" class="maps" style="width: 100%; height: 533px;"></div>
            </div>
        </div>
        <div id="blance-s2" class="one-half flat-maps-form2 formrequest2">
            <div class="title-section style2 color-white titlesize48">
                <h1 class="title"><span>Contactez</span> Nous.</h1>
                <div class="sub-title">
                   Envie de savoir plus sur nos produit ou passer une commande, Contactez nous. Nous attendons vos messages avec impatience.
                </div>
            </div>
            <div class="wrap-formrequest">
                <form id="contactform" class="contactform wrap-form style2 clearfix" method="post" action="./contact/contact-process2.php" novalidate="novalidate">
                    <span class="title-form">Je veux parler de:</span>
                    <span class="flat-input flat-select">
                        <select>
                            <option value="">Passer Une commande</option>
                            <option value="">Service Apres Vente</option>
                            <option value="">Autres</option>
                        </select>
                    </span>
                    <span class="flat-input"><input name="phone" type="text" value="" placeholder="Numéro De Téléphone" required="required"></span>
                    <span class="flat-input"><input name="name" type="text" value="" placeholder="Votre Nom" required="required"></span>
                    <span class="flat-input"><button name="submit" type="submit" class="flat-button" id="submit" title="Submit now">Envoyer<i class="fa fa-angle-double-right"></i></button></span>
                </form>
            </div>
        </div>
    </section>




<?php 
    include "./templates/footer.php";
    ?>