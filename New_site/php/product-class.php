<?php
//include './db.php';

class Product 
{
    var $productId;
    var $productCategoryId ;
    var $productName  ;
    var $productBrand ;
    var $productDescription;
    var $productImagesPath;
    var $productListPrice ;
    var $productIsDiscounted;
    var $productDiscountPrice;


    public static function fetchAll ()
    {
        global $db;
        $stmt = $db->prepare("SELECT * FROM products");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $product = new Product($row['ProductCategoryId'],$row['ProductName'],$row['ProductBrand'],$row['ProductDescription'],
                                    $row['ProductImagesPath'],$row['ProductListPrice'],$row['ProductIsDiscounted'],$row['ProductDiscountedPrice']) ;
            $product->setProductId($row['ProductId']);
            $productsArray[] = $product;
        }
        return $productsArray;
    }

    public static function fetchByCategoryID(int $ID)
    {
        global $db;
        $stmt = $db->prepare("SELECT * FROM products WHERE ProductCategoryId= :id");
        $stmt->bindParam(':id', $ID);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $product = new Product($row['ProductCategoryId'],$row['ProductName'],$row['ProductBrand'],$row['ProductDescription'],
                                    $row['ProductImagesPath'],$row['ProductListPrice'],$row['ProductIsDiscounted'],$row['ProductDiscountedPrice']) ;
            $productsArray[] = $product;
        }
        return $productsArray;
    }

    private function __construct( int $categoryId,string $name, string $brand,string $description,string $images,float $listPrice, int $isDiscounted, float $discountPrice ) 
    {
        $this->productCategoryId = $categoryId;
        $this->productName = $name;
        $this->productBrand = $brand;
        $this->productDescription = $description;
        $this->productImagesPath = $images;
        $this->productListPrice = $listPrice;
        $this->productIsDiscounted = $isDiscounted;
        $this->productDiscountPrice = $discountPrice;
    }

    public function getProductCategoryId()
    {
        return $this->productCategoryId;
    }

    public function getProductName() 
    {
        return $this->productName;
    }

    public function getProductBrand()
    {
        return $this->productBrand;
    }

    public function getProductDescription()
    {
        return $this->productDescription;
    }

    public function getProductMainImage()
    {
        return ("images/product/" . $this->productImagesPath . "/main.jpg");
    }

    public function getProductListPrice()
    {
        return $this->productListPrice;
    }

    public function getProductDiscountPrice() 
    {
        return $this->productDiscountPrice;
    }

    public function DisplayProductPrice()
    {
        if ($this->productIsDiscounted == 0)
        {
            echo '<div class="mount"> <span>' . $this->getProductListPrice() .'  Dt</span> </div>';
        }
        else 
        {
            echo '<div class="price-box">'.
             '<del><span class="amount"> ' . $this->getProductListPrice() .'  Dt </span> </del><br>'.
             '<ins><span class="mount">' . $this->getProductDiscountPrice() .'  Dt</span> </ins>'.
             ' </div>';
        }
    }
    public function getProductId()
    {
        return $this->productId;
    }
    private function setProductId (int $pId)
    {
        $this->productId = $pId;
    }

    







}
/*
$product = Product::fetchAll()[0];

$product->DisplayProductPrice();*/