<?php

//include_once "./db.php";



class Category
{
    public $categoryName = "";

    static public function fetchAll()
    {
        global $db;
        $stmt = $db->prepare("SELECT * FROM categories");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $categoriesArray[] = $row;
        }
        return $categoriesArray;
    }

    /*public function __construct($name)
    {
        $this->categoryName = $this->preparedata($name);
        global $db;
        $stmt = $db->prepare("INSERT INTO CATEGORIES (categoryName) VALUES (:name)");
        $stmt->bindParam('name', $this->categoryName);
        $stmt->execute();
    }*/


    /**
     * Gets categoryID of object from the database
     * @param void
     * @return int ID if found and 0 if not
     */
    private function getCategoryId()
    {
        global $db;
        $stmt = $db->prepare("SELECT categoryId from CATEGORIES where 'categoryName'=:name");
        $stmt->bindParam('name', $this->categoryName);
        $stmt->execute();
        $result = $stmt->fetch();
        if (is_array($result)) {
            return $result['categoryId'];
        } else {
            return 0;
        }
    }
    /**
     * updates database entry of current object
     * with new name given as argument
     * sets categoryname of object to new name argument
     * @param string newname : new category name
     * @return void
     */
    public function EditCategory($newname)
    {
        $newname = $this->prepare_data($newname);
        $this->categoryName = $newname;
        $id = $this->getCategoryId;
        $stmt = "UPDATE CATEGORIES SET 'categoyName'=:name WHERE 'categoryId'=:id";
        $stmt->bindParam('name', $this->categoryName);
        $stmt->bindParam('id', $id);
        $stmt->execute();

    }

    /**
     * finds object in database and deletes its entry
     * @param void
     * @return void
     */
    public function DeleteCategory()
    {
        $id = $this->getCategoryId();
        $stmt = "DELETE FROM CATEGORIES WHERE 'categoryId'=:id";
        $stmt->bindParam('id', $id);
        $stmt->execute();

        

    }

    /**
     * enlever les slash (/), quotations autres
     * caractères spéciaux pour protéger contre
     * l'injection SQL et autres
     * parametre : string $data
     * retour : string nettoyé*/
    private function prepare_data($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
?>