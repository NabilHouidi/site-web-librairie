<?php 
    include "./templates/header.php";
    ?>
     <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="page-title-heading">
                        <h1 class="title">Single Product</h1>
                    </div><!-- /.page-title-captions -->  
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                            <li><a href="shop.html">Shop</a></li>
                            <li>Singel Product</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /.page-title --> 

    <section class="flat-row product-single">
        <div class="container">
            <div class="margin-bottom-79">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="product-img">
                            <img src="images/product/d1.jpg" alt="image">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single margin-left25">
                            <h6 class="product-name">Economic Pluralism</h6>
                            <div class="price-box"> 
                                <ins><span class="amount">$980.00</span></ins>
                                <del><span class="amount">$1060.00</span></del>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <div class="flat-quantity ">
                                <form class="cart clearfix" method="post">
                                    <div class="quantity float-left">
                                        <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="input-text" size="4">
                                    </div>
                                    <input type="hidden" name="add-to-cart" value="191">
                                    <button class="flat-button ">ADD TO CART</button>
                                </form>
                            </div>
                            <ul class="product_meta">
                                <li><span>SKU:</span> 00012</li>
                                <li><span>Categories: </span><a href="#">Business,</a><a href="#">Tech,</a><a href="#">Work</a></li>
                                <li><span>Tags: </span><a href="#">man,</a><a href="#">t-shirt</a></li>
                            </ul>
                            <ul class="social-links">
                                <li><span class="title-link">Share Link:</span></li>
                                <li><a href="#"><i class="social_facebook"></i></a></li>
                                <li><a href="#"><i class="social_twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flat-tabs" data-effect="fadeIn">
                <ul class="menu-tab clearfix">
                    <li class="active"><a href="#">PRODUCT DESCRITION</a></li>
                    <li><a href="#">ADDITIONAL IMFORMATION</a></li>
                    <li><a href="#">CUSTOMER REVIEWS (02)</a></li>
                </ul><!-- /.menu-tab -->
                <div class="content-tab review">
                    <div class="content-inner inner-tab1">
                        <div class="tab-text1">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>                                        
                    </div>
                    <div class="content-inner inner-tab2">
                        <div class="tab-text1">
                            <ul class="product-style">
                                <li><span class="style">Color</span>:<span class="detail">Yellow</span></li>
                                <li><span class="style">Size</span>:<span class="detail">15X20cm</span></li>
                                <li><span class="style">Page</span>:<span class="detail">568</span></li>
                                <li><span class="style">Print</span>:<span class="detail">Color</span></li>
                            </ul>                                             
                        </div>                                        
                    </div>
                    <div class="content-inner inner-tab3">
                        <div class="tab-comment">
                            <ol class="comment-list">
                                <li class="comment">
                                    <article class="comment-body clearfix">        
                                        <div class="comment-author float-left">
                                            <img src="images/product/comment2.jpg" alt="image"> 
                                             <div class="start-pri text-center">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                        </div><!-- .comment-author -->
                                        <div class="comment-text">
                                            <div class="comment-metadata">
                                                <div class="comment-info">
                                                    <span class="date">27 Aug 2017</span>
                                                    <h6><a href="#">Jenney Kelley</a></h6> 
                                                </div>          
                                            </div><!-- .comment-metadata -->
                                            <div class="comment-content">
                                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.</p>
                                            </div><!-- .comment-content -->
                                        </div><!-- /.comment-text -->                       
                                    </article><!-- .comment-body -->
                                </li>
                                <li class="comment">
                                    <article class="comment-body clearfix">        
                                        <div class="comment-author float-left">
                                            <img src="images/product/comment1.jpg" alt="image"> 
                                            <div class="start-pri">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                        </div><!-- .comment-author -->
                                        <div class="comment-text">
                                            <div class="comment-metadata">
                                                <div class="comment-info">
                                                    <span class="date">27 Aug 2016</span>
                                                    <h6><a href="#">Brandon William</a></h6> 
                                                </div>          
                                            </div><!-- .comment-metadata -->
                                            <div class="comment-content">
                                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.</p>
                                            </div><!-- .comment-content -->
                                        </div><!-- /.comment-text -->                       
                                    </article><!-- .comment-body -->
                                </li>                                
                            </ol><!-- .comment-list -->  
                            <div class="comment-respond" id="respond">
                                <h2 class="comment-reply-title">Add Review</h2>
                                <form novalidate="" class="comment-form" id="commentform" method="post" action="#">
                                    <div class="wrap-input clearfix">
                                        <span class="add-review comment-notes">                                      
                                            <input type="text" placeholder="Name*" aria-required="true" size="30" value="" name="author" id="author">
                                        </span>
                                        <span class="add-review comment-form-email">          
                                            <input type="email" placeholder="Email*" size="30" value="" name="email" id="email">
                                        </span>
                                    </div>  
                                    <div class="start-pri">
                                        <span class="title">Your Rating:</span>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>                               
                                    <span class="comment-form-comment">
                                        <textarea class="comment-messages" tabindex="4" placeholder="Your Review" name="comment" required=""></textarea>
                                    </span> 
                                </form>
                            </div><!-- /.comment-respond -->                                          
                        </div>
                    </div>
                </div><!-- /.content-tab -->
            </div>
        </div>
    </section>

    <section class="flat-row section-product single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section style3 left">
                        <h1 class="title">Related Products</h1>
                    </div>
                </div>
            </div>
            <div class="wrap-product clearfix">
                <div class="product">
                    <div class="box-product">
                        <div class="featured-product">
                            <a href="shop-single.html"><img src="images/product/4.jpg" alt="image"></a>
                        </div>
                        <div class="content-product text-center">
                            <div class="name">
                                <span>Economic Pluralism</span>
                            </div>
                            <div class="mount">
                                <span>$199.00</span>
                            </div>
                            <div class="btn-card">
                                <a href="shop-single.html" class="flat-button style2">add to card</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product">
                    <div class="box-product">
                        <div class="featured-product">
                            <a href="shop-single.html"><img src="images/product/5.jpg" alt="image"></a>
                        </div>
                        <div class="content-product text-center">
                            <div class="name">
                                <span>Economic Pluralism</span>
                            </div>
                            <div class="mount">
                                <span>$199.00</span>
                            </div>
                            <div class="btn-card">
                                <a href="shop-single.html" class="flat-button style2">add to card</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product">
                    <div class="box-product">
                        <div class="featured-product">
                            <a href="shop-single.html"><img src="images/product/6.jpg" alt="image"></a>
                        </div>
                        <div class="content-product text-center">
                            <div class="name">
                                <span>Economic Pluralism</span>
                            </div>
                            <div class="mount">
                                <span>$199.00</span>
                            </div>
                            <div class="btn-card">
                                <a href="shop-single.html" class="flat-button style2">add to card</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
    include "./templates/footer.php"
?>